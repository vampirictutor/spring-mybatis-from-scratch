#!/bin/bash 

# todo: automately fetch jar filename
JARFILE=build/libs/spring-mybatis-from-scratch-0.0.1-SNAPSHOT.jar
CMD="java -jar $JARFILE"


function wait_cmd {
    max_attempts=10
    delay=1

    attempt=0

    while [ $attempt -lt $max_attempts ]; do
        if pgrep -f "$CMD" > /dev/null; then
            echo "Process $CMD found. Killing it..."
            pkill -f "$CMD"
            sleep $delay
            ((attempt++))
        else
            # echo "Process $CMD not found. go on..."
            return 0  
        fi
    done

    echo "Fail to end '$CMD'"
    return 1
}

build_cmd="./gradlew build -x test"

if ! $build_cmd ; then
    echo "Build failed."
    exit 1
fi

wait_cmd $CMD

$CMD

### just memo
# $ nohup socat  TCP-LISTEN:8000,reuseaddr,fork   tcp:127.0.0.1:5000 &


