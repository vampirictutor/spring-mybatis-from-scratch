package com.bizplus.springmybatisdemo.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.ui.Model;

import com.bizplus.springmybatisdemo.domain.Todo;
import com.bizplus.springmybatisdemo.mapper.TodoMapper;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@Controller
public class PortalController {

    static Logger logger = LoggerFactory.getLogger(PortalController.class);

    @Autowired
    private TodoMapper todoMapper;

    @RequestMapping(path = {"/", "/index"})
    public String index(
            HttpServletRequest  request
			, Model model
            ) {

        List<Todo> todos = todoMapper.listAll();
        /*
        for (Todo entry : todos)
        {
            String s = String.format("#%d %s - %s"
                , entry.id() 
                , entry.title() 
                , entry.details());

            logger.debug(s);
        }
        */
        model.addAttribute("todos", todos);
        return "list"; 
    } 

    @RequestMapping(path = {"/new"})
    public String newTodo (
            HttpServletRequest  request
            , @RequestParam(name="title", required=true) String title
            , @RequestParam(name="details", required=true) String details
			, Model model
            ) {
        
        Todo newTodo = new Todo(
                title,
                details
                );

        todoMapper.insert(newTodo); // 新しいTodoをインサートする

        return "redirect:/"; 
    }
}

