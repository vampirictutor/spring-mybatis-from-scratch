package com.bizplus.springmybatisdemo.mapper;

import  com.bizplus.springmybatisdemo.domain.Todo;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TodoMapper {

    void insert(Todo todo);

    Todo select(int id);

    List<Todo> listAll();
}

