package com.bizplus.springmybatisdemo.domain;

public record Todo (
    int id,
    String title,
    String details,
    boolean finished)
{

    public Todo (
            String title,
            String details
            )
    {
        this(-1, title, details, false);
    }
}

