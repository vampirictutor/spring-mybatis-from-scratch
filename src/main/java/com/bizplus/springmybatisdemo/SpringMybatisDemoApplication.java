package com.bizplus.springmybatisdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@SpringBootApplication
public class SpringMybatisDemoApplication {

    static Logger logger = LoggerFactory.getLogger(SpringMybatisDemoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringMybatisDemoApplication.class, args);
        logger.debug("Yes, let's do something");
	}

}
