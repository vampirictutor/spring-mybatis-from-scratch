package com.bizplus.springmybatisdemo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest; // @SpringBootTest
import org.springframework.test.context.ActiveProfiles;      // @ActiveProfiles

import org.springframework.beans.factory.annotation.Autowired; // @Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc; //@AutoConfigureMockMvc

import org.springframework.test.web.servlet.MockMvc; // MockMvc mvc;

import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import org.springframework.test.context.jdbc.Sql; // @Sql

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import org.hamcrest.Matchers;
// import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("localtest")
class SpringMybatisDemoApplicationTests {

    static Logger logger = LoggerFactory.getLogger(SpringMybatisDemoApplicationTests.class);
	
    @Autowired
    MockMvc mvc;

    @Test
	void contextLoads() {
        // int a = 1/0;   // make the test fail.

        logger.debug("Hello, test."); // shows in build/reports/tests/index.html
	} 

    @Test
    @Sql("classpath:test_stuff/SpringMybatisDemoApplicationTests/list1.sql")
	void list() throws Exception
    {
        this.mvc.perform(MockMvcRequestBuilders.get("/"))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(
                    MockMvcResultMatchers.model().attribute(
                        "todos",   Matchers.hasSize(1) 
                    )
            )
            ;
    }
}
