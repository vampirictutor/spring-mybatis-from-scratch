#!/bin/bash -e
#

# to install 'yq'
# sudo pip install yq
#

### 1. tweak application.yml
PropFile=src/main/resources/application.yml
BakFile="$PropFile.bak"
TempFile="prop.temp"

cp $PropFile $BakFile
yq -Y '.spring.profiles.active = "aws"' $PropFile > $TempFile
mv $TempFile $PropFile

### 2. imprint version to portal page
CurrentVersion=`git describe`
echo "Suggested version: $CurrentVersion"

PortalTemplate=src/main/resources/templates/list.html
PortalBak="$PortalTemplate.bak"

sed -i.bak "s/##VERSION##/$CurrentVersion/" $PortalTemplate

### 3. build bar
./gradlew bootJar

### 4. restore fiels
mv $BakFile $PropFile 
mv $PortalBak $PortalTemplate

